#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto LiLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto WiLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");

  const auto uBaseColorTexture = glGetUniformLocation(glslProgram.glId(),"uBaseColorTexture");

  const auto uBaseColorFactorLocation = glGetUniformLocation(glslProgram.glId(),"uBaseColorFactor");

  const auto uMetallicFactorLocation = glGetUniformLocation(glslProgram.glId(),"uMetallicFactor");
  const auto uRoughnessFactorLocation = glGetUniformLocation(glslProgram.glId(),"uRoughnessFactor");

  const auto uMetallicRoughnessTextureLocation = glGetUniformLocation(glslProgram.glId(),"uMetallicRoughnessTexture");

  const auto uEmissiveTexture =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");


  const auto uOcclusionFactor = glGetUniformLocation(glslProgram.glId(),"uOcclusionFactor");
  const auto uOcclusionTexture = glGetUniformLocation(glslProgram.glId(),"uOcclusionTexture");
  bool occlusionFLAG = 0;
  const auto  uOcclusionFLAG= glGetUniformLocation(glslProgram.glId(),"uOcclusionFLAG");


  glm::vec3 lightDirection = glm::vec3(10,10,10);
  glm::vec3 lightIntensity = glm::vec3(10,10,10);
  bool fromCamera = false;// checkBox to send light from camera or not




  tinygltf::Model model;
  // TODO Loading the glTF file
  loadGltfFile(model);
    // Build projection matrix
  auto maxDistance = 500.f; // TODO use scene bounds instead to compute this
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);
  
  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  //FirstPersonCameraController cameraController{
  //TrackballCameraController cameraController
  std::unique_ptr<CameraController> cameraController= std::make_unique<TrackballCameraController>( m_GLFWHandle.window(), 0.5f * maxDistance);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    cameraController->setCamera(
        Camera{glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0)});
  }
  
  //setup camera
  glm::vec3 bboxMin;
  glm::vec3 bboxMax;
  computeSceneBounds( model, bboxMin, bboxMax);
  cameraController->setCamera(Camera{(bboxMax-bboxMin)/2.f+(bboxMax-bboxMin),(bboxMax-bboxMin)/2.f,glm::vec3(0,1.f,0)});
  //cameraController->setSpeed (0.1*maxDistance);

  //Texture sampling
  std::vector<GLuint> textureObjects = createTextureObjects(model);
  GLuint whiteTexture;
  float white[]={1,1,1,1};
  glGenTextures(1,&whiteTexture);
  // Bind texture object to target GL_TEXTURE_2D:
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  // Set image data:
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0,
          GL_RGB, GL_FLOAT, white);
  // Set sampling parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // Set wrapping parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  glBindTexture(GL_TEXTURE_2D, 0);





  // TODO Creation of Buffer Objects
  std::vector<GLuint> bufferObjects = createBufferObjects(model);
  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  std::vector<GLuint> vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);
  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  const auto bindMaterial = [&](const auto materialIndex) 
  {
    
    
    GLint textureObject;
    if(materialIndex>=0)
    {
      textureObject = whiteTexture;
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      const auto &metallicRoughnessTexture = pbrMetallicRoughness.metallicRoughnessTexture;
      const auto &occlusionTexture = material.occlusionTexture;
      if (uBaseColorFactorLocation >= 0) 
      {
        glUniform4f(uBaseColorFactorLocation,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
              
      }
      if(uMetallicFactorLocation>=0)
      {
        glUniform1f(uMetallicFactorLocation,(float)pbrMetallicRoughness.metallicFactor);
      }
      if(uRoughnessFactorLocation>=0)
      {
        glUniform1f(uRoughnessFactorLocation,(float)pbrMetallicRoughness.roughnessFactor);
      }
      

      if(metallicRoughnessTexture.index>=0)
      {
        const auto &texture = model.textures[metallicRoughnessTexture.index];
        if(texture.source >= 0)
        {
          textureObject = textureObjects[texture.source];

        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);

      }


      if(pbrMetallicRoughness.baseColorTexture.index >=0)
      {
        const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
        if(texture.source >= 0)
        {
          textureObject = textureObjects[texture.source];

        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTexture, 0);



      }

      
      if(uOcclusionFactor>=0)
      {
        glUniform1f(uOcclusionFactor,(float)occlusionTexture.strength);
      }

      if(uOcclusionTexture>=0)
      {
        const auto &texture = model.textures[occlusionTexture.index];
        if(texture.source>=0)
        {
          textureObject = textureObjects[texture.source];
        }
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D,textureObject);
        glUniform1i(uOcclusionTexture,3);
      }
      


      
      if (uEmissiveFactor >= 0) 
      {
        glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      if (uEmissiveTexture >= 0) 
      {
        auto textureObject = 0;
        if (material.emissiveTexture.index >= 0) 
        {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uEmissiveTexture, 2);
      }

    }
    else
    {
      if (uBaseColorFactorLocation >= 0) 
      {
        glUniform4f(uBaseColorFactorLocation, 1, 1, 1, 1);
      }

      if (uBaseColorTexture >= 0) 
      {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTexture, 0);

      }

      if(uMetallicFactorLocation>=0)
      {
        glUniform1f(uMetallicFactorLocation,0);
      }

      if(uRoughnessFactorLocation>=0)
      {
        glUniform1f(uRoughnessFactorLocation,0);
      }
      if(uMetallicRoughnessTextureLocation>=0)
      {

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);

      }

      if (uEmissiveFactor >= 0) 
      {
        glUniform3f(uEmissiveFactor, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTexture >= 0) 
      {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uEmissiveTexture, 2);
      }

      if(uOcclusionFactor>=0)
      {
        glUniform1f(uOcclusionFactor,0.f);
      }

      if(uOcclusionTexture>=0)
      {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D,whiteTexture);
        glUniform1i(uOcclusionTexture,3);

      }





    }
    

  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();



    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          tinygltf::Node node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if(LiLocation>=0){ glUniform3f(LiLocation,lightIntensity.x,lightIntensity.y,lightIntensity.z);}

          if(WiLocation>=0)
          {
            if(fromCamera)
            {
              glUniform3f(WiLocation,0,0,1);
            }
            else
            {
            const auto lightDirectionInViewSpace = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
            glUniform3f(WiLocation,lightDirectionInViewSpace.x,lightDirectionInViewSpace.y,lightDirectionInViewSpace.z);
            }
          }

          if (node.mesh >= 0) 
          {
            glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
            glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(normalMatrix));

            const auto &mesh = model.meshes[node.mesh];
            const auto vaoRange = meshIndexToVaoRange[node.mesh];
            for (int primitiveIdx = 0; primitiveIdx < mesh.primitives.size();primitiveIdx++) 
            {
              bindMaterial(mesh.primitives[primitiveIdx].material);
              glBindVertexArray(vertexArrayObjects[vaoRange.begin + primitiveIdx]);
              const auto primitive = mesh.primitives[primitiveIdx];
              if (primitive.indices >= 0) 
              {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                glDrawElements(primitive.mode, accessor.count, accessor.componentType,(const GLvoid *)(accessor.byteOffset + bufferView.byteOffset));
              } 
              else 
              {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }

            }
          }
          for (int childIdx : node.children) 
          {
            drawNode(childIdx, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // TODO Draw all nodes
      for (int nodeIdx = 0;nodeIdx < model.scenes[model.defaultScene].nodes.size(); nodeIdx++) 
      {
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };


  if(!m_OutputPath.empty())
  {
    std::vector<unsigned char> pixels(m_nWindowWidth,m_nWindowHeight*3);
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {drawScene(cameraController->getCamera());});
    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }
  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
        static int cameraControllerType = 0;
        const auto cameraControllerTypeChanged =
            ImGui::RadioButton("Trackball", &cameraControllerType, 0) ||
            ImGui::RadioButton("First Person", &cameraControllerType, 1);
        if (cameraControllerTypeChanged) 
        {
          const auto currentCamera = cameraController->getCamera();
          if (cameraControllerType == 0) {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          } else {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }
        ImGui::Checkbox("ambiant Occlusion", &occlusionFLAG);
        if(occlusionFLAG == 1)
        {
          glUniform1i(uOcclusionFLAG ,1);
        }
        else{glUniform1i(uOcclusionFLAG,0);}

        if(ImGui::CollapsingHeader("Light"))
        {
            static float ligthAngle1 = 0.0f,ligthAngle2 = 0.0f;
            ImGui::SliderAngle("Light Angle 1", &ligthAngle1);
            ImGui::SliderAngle("Light Angle 2", &ligthAngle2);
            float angle1Rad = glm::radians(ligthAngle1);
            float angle2Rad = glm::radians(ligthAngle2);
            lightDirection = glm::vec3(sin(ligthAngle1)*cos(ligthAngle2),cos(ligthAngle1),sin(ligthAngle1)*cos(ligthAngle2));
            
            static float lightColor[3] = { 1.0f, 0.0f, 0.2f };
            static float intensityValue;
            ImGui::ColorEdit3("Light Color", lightColor);
            ImGui::SliderFloat("Light intensity", &intensityValue, 0 , 100,"%.0f");
            lightIntensity = intensityValue*glm::vec3(lightColor[0],lightColor[1],lightColor[2]);
        
            
            ImGui::Checkbox("From Camera", &fromCamera);

        }



      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  std::string err;
  std::string warn;
  tinygltf::TinyGLTF loader;
  std::string path;

  path = m_gltfFilePath.string();
  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, path);

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return -1;
  }

  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{

  std::vector<GLuint> bufferObject(model.buffers.size(), 0);
  glGenBuffers(model.buffers.size(), bufferObject.data());

  for (uint bufferIdx = 0; bufferIdx < model.buffers.size(); bufferIdx++) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObject[bufferIdx]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[bufferIdx].data.size(),
        model.buffers[bufferIdx].data.data(), 0);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferObject;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  std::vector<GLuint> vertexArrayObjects;
  const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

  /// ndeed, we should have a VAO for each primitive of the file. But the top
  /// level description of geometry is made with "meshes". And each mesh can
  /// have multiple "primitives".
  for (uint meshIdx = 0; meshIdx < model.meshes.size(); meshIdx++) {
    const auto vaoOffset = vertexArrayObjects.size();
    vertexArrayObjects.resize(
        vaoOffset + model.meshes[meshIdx].primitives.size());
    meshIndexToVaoRange.push_back(
        VaoRange{vaoOffset, model.meshes[meshIdx].primitives.size()});
    for (uint primitivesIdx = 0;
         primitivesIdx < model.meshes[meshIdx].primitives.size();
         primitivesIdx++) {
      glGenVertexArrays(1, &vertexArrayObjects[vaoOffset + primitivesIdx]);
      glBindVertexArray(vertexArrayObjects[vaoOffset + primitivesIdx]);
      const auto primitive = model.meshes[meshIdx].primitives[primitivesIdx];
      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator !=
            end(primitive
                    .attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor =
              model.accessors[accessorIdx]; // TODO get the correct
                                            // tinygltf::Accessor from
                                            // model.accessors
          const auto &bufferView =
              model.bufferViews[accessor.bufferView]; // TODO get the correct
                                                      // tinygltf::BufferView
                                                      // from model.bufferViews.
                                                      // You need to use the
                                                      // accessor
          const auto bufferIdx =
              bufferView.buffer; // TODO get the index of the buffer used by the
                                 // bufferView (you need to use it)

          const auto bufferObject =
              model.buffers[bufferIdx]; // TODO get the correct buffer object
                                        // from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
          // the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset =
              accessor.byteOffset +
              bufferView.byteOffset; // TODO Compute the total byte offset using
                                     // the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
              accessor.componentType, GL_FALSE,
              (GLsizei)(bufferView.byteStride), (const GLvoid *)byteOffset);

          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't
          // forget the cast).
        }
      }

      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator !=
            end(primitive
                    .attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor =
              model.accessors[accessorIdx]; // TODO get the correct
                                            // tinygltf::Accessor from
                                            // model.accessors
          const auto &bufferView =
              model.bufferViews[accessor.bufferView]; // TODO get the correct
                                                      // tinygltf::BufferView
                                                      // from model.bufferViews.
                                                      // You need to use the
                                                      // accessor
          const auto bufferIdx =
              bufferView.buffer; // TODO get the index of the buffer used by the
                                 // bufferView (you need to use it)

          const auto bufferObject =
              model.buffers[bufferIdx]; // TODO get the correct buffer object
                                        // from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
          // the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset =
              accessor.byteOffset +
              bufferView.byteOffset; // TODO Compute the total byte offset using
                                     // the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
              accessor.componentType, GL_FALSE,
              (GLsizei)(bufferView.byteStride), (const GLvoid *)byteOffset);

          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't
          // forget the cast).
        }
      }
      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator !=
            end(primitive
                    .attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor =
              model.accessors[accessorIdx]; // TODO get the correct
                                            // tinygltf::Accessor from
                                            // model.accessors
          const auto &bufferView =
              model.bufferViews[accessor.bufferView]; // TODO get the correct
                                                      // tinygltf::BufferView
                                                      // from model.bufferViews.
                                                      // You need to use the
                                                      // accessor
          const auto bufferIdx =
              bufferView.buffer; // TODO get the index of the buffer used by the
                                 // bufferView (you need to use it)

          const auto bufferObject =
              model.buffers[bufferIdx]; // TODO get the correct buffer object
                                        // from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
          // the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset =
              accessor.byteOffset +
              bufferView.byteOffset; // TODO Compute the total byte offset using
                                     // the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
              accessor.componentType, GL_FALSE,
              (GLsizei)(bufferView.byteStride), (const GLvoid *)byteOffset);

          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't
          // forget the cast).
        }
      }
      if (primitive.indices >= 0) {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
    }
  }
  glBindVertexArray(0);
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const 
{

  std::vector<GLuint> texObject(model.textures.size());



  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  for(uint texIndex = 0 ; texIndex < model.textures.size();texIndex++ )
  {
    // Generate the texture object:
    glGenTextures(1, &texObject[texIndex]);

    // Bind texture object to target GL_TEXTURE_2D:
    glBindTexture(GL_TEXTURE_2D, texObject[texIndex]);
    // Set image data:
    const auto &texture = model.textures[texIndex]; // get i-th texture
    assert(texture.source >= 0); // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image

    // fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());
    
    const auto &sampler =
            texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
      sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
      sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
      sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
      sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
      sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }


    glBindTexture(GL_TEXTURE_2D, 0);
  }

  return texObject;
}