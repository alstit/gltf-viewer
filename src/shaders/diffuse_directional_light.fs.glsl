#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLi;
uniform vec3 uWi;

out vec3 fColor;

vec3 light(vec3 normal)
{
    return vec3(1/3.14,1/3.14,1/3.14)*uLi*dot(normal,uWi/length(uWi));
}



void main()
{
   // Need another normalization because interpolation of vertex attributes does not maintain unit length
   vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
   //fColor = viewSpaceNormal;

   fColor = light(viewSpaceNormal);
}